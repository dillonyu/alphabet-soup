import java.io.File;  
import java.io.FileNotFoundException;  
import java.util.Scanner; 
import java.util.ArrayList;

public class Solution {
    public static void main(String[] args) {
        try {
            File input = new File("file.txt");
            Scanner reader = new Scanner(input);
            String data = reader.nextLine();
            String[] dimensions = data.split("");
            int rows = Integer.parseInt(dimensions[0]);
            int columns = Integer.parseInt(dimensions[2]);
            char[][] matrix = new char[rows][columns];
            for (int i = 0; i < rows; i++) {
                data = reader.nextLine();
                String[] row = data.split(" ");
                for (int j = 0; j < columns; j++) {
                    matrix[i][j] = row[j].charAt(0);
                }
            }

            ArrayList<String> words = new ArrayList<>();
            while (reader.hasNextLine()) {
                data = reader.nextLine();
                words.add(data);
            }
    
            ArrayList<String> solutions = new ArrayList<>();
            for (String word: words) {
                solutions.add(getSolution(word, matrix));
            }

            for (String solution: solutions) {
                System.out.println(solution);
            }
            reader.close();
        } 
        catch (FileNotFoundException e) {
          System.out.println("An error occurred.");
          e.printStackTrace();
        }
    }

    /**
     * Given a word and the grid, finds the solution and returns the coordinates of the first
     * letter and the last letter.
     * @param word
     * @param grid
     * @return
     */
    private static String getSolution(String word, char[][] grid) {
        int rows = grid.length;
        int cols = grid[0].length;

        for (int i = 0; i < rows; i++) {
            for (int j = 0; j < cols; j++) {
                if ((word.charAt(0) == grid[i][j])) {
                    String result = search(grid, i, j, word, 1);
                    if (!result.isEmpty()) {
                        return word + " " + i + ":" + j + " " + result;
                    }
                }
            }
        }
        return "";
    }

    /**
     * Searches the input grid starting from the input row and column for the input word. Returns the coordinate of the last
     * letter if the word is found, and an empty string otherwise.
     * @param grid
     * @param row
     * @param col
     * @param word
     * @param index
     * @return
     */
    private static String search(char[][] grid, int row, int col, String word, int index) {
        int origRow = row;
        int origCol = col;
        // Search Left
        while (inBounds(row - 1, col, grid)) {
            if (grid[row - 1][col] == word.charAt(index) && index == word.length() - 1) {
                return row + ":" + col;
            }
            if (grid[row - 1][col] != word.charAt(index)) {
                break;
            }
            row--;
            index++;
        }

        row = origRow;
        col = origCol;
        index = 1;

        // Search Right
        while (inBounds(row + 1, col, grid)) {
            if (grid[row + 1][col] == word.charAt(index) && index == word.length() - 1) {
                return (row + 1) + ":" + col;
            }
            if (grid[row + 1][col] != word.charAt(index)) {
                break;
            }
            row++;
            index++;
        }

        row = origRow;
        col = origCol;
        index = 1;

        // Search Up
        while (inBounds(row, col - 1, grid)) {
            if (grid[row][col - 1] == word.charAt(index) && index == word.length() - 1) {
                return row + ":" + (col - 1);
            }
            if (grid[row][col - 1] != word.charAt(index)) {
                break;
            }
            col--;
            index++;
        }

        row = origRow;
        col = origCol;
        index = 1;

        // Search Down
        while (inBounds(row, col + 1, grid)) {
            if (grid[row][col + 1] == word.charAt(index) && index == word.length() - 1) {
                return row + ":" + (col + 1);
            }
            if (grid[row][col + 1] != word.charAt(index)) {
                break;
            }
            col++;
            index++;
        }

        row = origRow;
        col = origCol;
        index = 1;

        // Search Up-Left
        while (inBounds(row - 1, col - 1, grid)) {
            if (grid[row - 1][col - 1] == word.charAt(index) && index == word.length() - 1) {
                return (row - 1) + ":" + (col - 1);
            }
            if (grid[row - 1][col - 1] != word.charAt(index)) {
                break;
            }
            row--;
            col--;
            index++;
        }

        row = origRow;
        col = origCol;
        index = 1;

        // Search Up-Right
        while (inBounds(row - 1, col + 1, grid)) {
            if (grid[row - 1][col + 1] == word.charAt(index) && index == word.length() - 1) {
                return (row - 1) + ":" + (col + 1);
            }
            if (grid[row - 1][col + 1] != word.charAt(index)) {
                break;
            }
            row--;
            col++;
            index++;
        }

        row = origRow;
        col = origCol;
        index = 1;

        // Search Down-Left
        while (inBounds(row + 1, col - 1, grid)) {
            if (grid[row + 1][col - 1] == word.charAt(index) && index == word.length() - 1) {
                return (row + 1) + ":" + (col - 1);
            }
            if (grid[row + 1][col - 1] != word.charAt(index)) {
                break;
            }
            row++;
            col--;
            index++;
        }

        row = origRow;
        col = origCol;
        index = 1;

        // Search Down-Right
        while (inBounds(row + 1, col + 1, grid)) {
            if (grid[row + 1][col + 1] == word.charAt(index) && index == word.length() - 1) {
                return (row + 1) + ":" + (col + 1);
            }
            if (grid[row + 1][col + 1] != word.charAt(index)) {
                break;
            }
            row++;
            col++;
            index++;
        }
        return "";
    }

    /**
     * Checks if the given row and column are within the bounds of the grid
     * @param row 
     * @param col
     * @param grid
     * @return
     */
    private static boolean inBounds(int row, int col, char[][] grid) {
        return row >= 0 && row < grid.length && col >= 0 && col < grid[0].length;
    }
}